$(function() {

// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.main-menu').fadeToggle(200);
});


// Бургер меню на моб. версии
if ($(window).width() < 768) {
   $(".main-menu a").click(function(){
    $('.burger-menu').toggleClass("active");
    $('.main-menu').fadeToggle(200);
  });
}


// FansyBox
 $('.fancybox').fancybox({});



// Аккордеон
$('.faq-header').click(function(){

if(!($(this).next().hasClass('active'))){
  $('.faq-body').slideUp().removeClass('active');
  $(this).next().slideDown().addClass('active');
}else{
  $('.faq-body').slideUp().removeClass('active');
};

if(!($(this).parent('.faq-item').hasClass('active'))){
  $('.faq-item').removeClass("active");
  $(this).parent('.faq-item').addClass("active");
}else{
  $(this).parent('.faq-item').removeClass("active");

};

});


// Позиция на карте карты
var leftPosition = ($(window).width() - $('.container').width())/2
$('.map-overlay').css('left',leftPosition)



// Скролл
$(".main-menu").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top}, 1500);
  });


})